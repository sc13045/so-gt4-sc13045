#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void evaluar(int n, double *coeficiente){
    double x, resultado=0.00, temp;

    printf("Ingrese el punto X a evaluar: ");
    printf("\t");
    scanf("%lf", &x);

    for (int i = 0; i < n+1; i++) { 
        temp= pow (x, n-i);
        resultado=resultado+(coeficiente[i]*temp);
    }
    
    printf("\nResultado : %.2lf",resultado);
    printf("\n");      
}
int main(){
    int n;
    double ingreso,*coeficiente;
    printf("Ingrese grado del polinomio: ");
    scanf("%d", &n);

   
    coeficiente =(double* ) malloc(n *sizeof(int)+1);

    
    for (int i = 0; i < n+1; i++){
        printf("Ingrese coeficiente para X^%d",n-i);
        printf("\t");
        scanf("%lf", &ingreso);
        coeficiente[i] = ingreso;
    }

   
    printf("Polinomio ingresado: \n");
    for (int i = 0; i < n+1; i++){
        printf(" %.2lf", coeficiente[i]);
        printf(" X^%d",n-i);
    }

    printf("\n");
    evaluar(n,coeficiente);
    return 0;

}
