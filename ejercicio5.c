#include <stdio.h>
#include <stdlib.h>



void sort(int n, int *numeros){
    int temp;
    for (int i = 0; i < n; i++) { 
        for (int j = i + 1; j < n; j++) { 
            if(*(numeros + j) < *(numeros + i)){
                temp = *(numeros + i); 
                *(numeros + i) = *(numeros + j); 
                *(numeros + j) = temp; 
            }
        } 
    }
   
    printf("Numeros despues de ordenarlos \n");
    for (int i = 0; i < n; i++) {
        printf(" %d ", *(numeros + i)); 
    } 
    printf("\n");       }

int main(){
    int n, *total;
    printf("\nOrdenando numeros :\n"); 
	printf("------------------------------------------------------\n");	  
    printf("Ingrese cuantos numeros desea ordenar: ");
    scanf("%d", &n);
    printf("------------------------------------------------------\n");	  

   
    total =(int* ) malloc(n *sizeof(int));

    
    for (int i = 0; i < n; i++){
        total[i] = rand() % 100;
    }

   
    printf("Numeros antes de ordenarlos: \n");
    for (int i = 0; i < n; i++){
        printf(" %d ", *(total + i));
    }

    printf("\n");
    sort(n, total);
    return 0;
}

